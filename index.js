const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');




///////////// SOLUTION 1

function generateFullName(){
	spanFullName.innerHTML = txtFirstName.value + '\xa0' + txtLastName.value;
	return;
};

txtFirstName.addEventListener('keyup', (event)=>{
	generateFullName();
});

txtLastName.addEventListener('keyup', (event)=>{
	generateFullName();
});



//////////////////// SOLUTION 2

/*function generateFullName(){
	txtFirstName.addEventListener('keyup', (event)=>{
		spanFullName.innerHTML = txtFirstName.value
	});

	txtLastName.addEventListener('keyup', (event)=>{
		spanFullName.innerHTML = txtFirstName.value + '\xa0' + txtLastName.value
	});
};

generateFullName();*/





/////////////////////////////


txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
});

	

